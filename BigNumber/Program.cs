// See https://aka.ms/new-console-template for more information

using BigNumber;

var isContinue = false;
do
{
    Console.Write("Input the first number: ");
    var firstNumber = Console.ReadLine() ?? "0";
    Console.Write("Input the second number: ");
    var secondNumber = Console.ReadLine() ?? "0";

    var result = MyBigNumber.Sum(firstNumber, secondNumber);

    Console.WriteLine($"{firstNumber} + {secondNumber} = {result}");
    
    Console.WriteLine("------------");
    Console.Write("Do you want to continue: Y/N");
    isContinue = Console.ReadLine()?.ToLower() == "y";
} while (isContinue);
