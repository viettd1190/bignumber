namespace BigNumber;

public static class MyBigNumber
{
    public static string Sum(string a, string b)
    {
        if (a.Length < b.Length) (a, b) = (b, a);

        var firstArray = a.ConvertToIntArray();
        var secondArray = b.ConvertToIntArray();

        var resultArray = new int[firstArray.Length + 1];
        var plus = 0;
        for (var i = 0; i < firstArray.Length; i++)
        {
            var firstNumber = firstArray[i];
            var secondNumber = 0;
            if (secondArray.Length >= i + 1) secondNumber += secondArray[i];
            
            var c = firstNumber + secondNumber + plus;
            
            if (plus==0)
            {
                Console.WriteLine(i != firstArray.Length - 1
                    ? $"{firstNumber} + {secondNumber} = {c} {(c >= 10 ? "plus 1" : "")}"
                    : $"{firstNumber} + {secondNumber} = {c} ");
            }
            else
            {
                Console.WriteLine(i != firstArray.Length - 1
                    ? $"{firstNumber} + {secondNumber} + 1 = {c} {(c >= 10 ? "plus 1" : "")}"
                    : $"{firstNumber} + {secondNumber} + 1 = {c}");
            }
            
            if (c >= 10)
            {
                c -= 10;
                plus = 1;
            }
            else
            {
                plus = 0;
            }

            resultArray[i] = c;
            
        }

        if (plus == 0) resultArray = resultArray.SkipLast(1).ToArray();
        else resultArray[^1] = 1;
        Array.Reverse(resultArray);

        return string.Join("", resultArray);
    }

    private static int[] ConvertToIntArray(this string source)
    {
        var output = source.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
        Array.Reverse(output);
        return output;
    }
}
